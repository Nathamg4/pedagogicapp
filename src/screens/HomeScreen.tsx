import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  ImageSourcePropType,
} from "react-native";
import { FlatList, ScrollView } from "react-native-gesture-handler";

import { useNavigation } from "../utils";

import { ButtonWithIcon, CategoryCard, NoticeCard } from "../components";
import { Notice } from "../redux";

const screenWidth = Dimensions.get("screen").width;
const _HomeScreen = () => {
  const { navigate } = useNavigation();

  const categories = [
    {
      id: "1",
      title: "Bienestar Estudiantil",
      icon: require("../images/yoga.png"),
    },
    { id: "2", title: "Universidad", icon: require("../images/uni.png") },
    { id: "3", title: "Biblioteca", icon: require("../images/bibli.png") },
  ];
  const notices: Notice[] = [
    {
      _id: "1",
      title: "DESARROLLO",
      subtitle: "Aplicación PedagogicApp en desarrollo",
      summary:
        "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum...",
      info: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      date: "26 de mayo de 2021",
      autor: "Carlos Hernán López Ruiz",
      image: require("../images/pantalla.png") as ImageSourcePropType,
    },
    {
      _id: "2",
      title: "DISEÑO",
      subtitle: "Diseño página web Cinndet",
      summary: "",
      info: "as",
      date: "31 de abril de 2021",
      autor: "Juan Felipe Lucena Castro",
      image: require("../images/pan2.png") as ImageSourcePropType,
    },
    {
      _id: "3",
      title: "TRABAJO EN EQUIPO",
      subtitle: "Reunión semanal equipo cinndet",
      summary: "",
      info: "as",
      date: "31 de abril de 2021",
      autor: "Juan Felipe Lucena Castro",
      image: require("../images/pan3.png") as ImageSourcePropType,
    },
  ];

  const onTapNotice = (item: Notice) => {
    navigate("NoticePage", { notice: item });
  };

  const onTapBack = () => {
    navigate("HomePage");
  };

  return (
    <View style={styles.container}>
      <View style={styles.cuadro}></View>

      <View style={styles.navigation}>
        <Text
          style={{
            color: "white",
            fontSize: 30,
            marginLeft: 20,
            marginTop: 60,
          }}
        >
          Explorar
        </Text>
        <View style={styles.absolute}>
          <ButtonWithIcon
            icon={require("../images/menu.png")}
            onTap={onTapBack}
            width={42}
            height={42}
          />
        </View>
      </View>
      <View style={styles.body}>
        <ScrollView>
          <View>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={categories}
              renderItem={({ item }) => (
                <CategoryCard
                  item={item}
                  onTap={() => {
                    alert("Próximamente solo en PedagogicApp");
                  }}
                />
              )}
              keyExtractor={(item) => `${item.id}`}
            />
          </View>
          <View>
            <Text
              style={{
                fontSize: 13,
                fontWeight: "600",
                color: "#003E8F",
                marginHorizontal: 20,
                marginVertical: 20,
              }}
            >
              ¿Qué hay de nuevo en la universidad?
            </Text>
          </View>
          <View>
            {notices.map((item, index) => {
              return <NoticeCard key={index} item={item} onTap={onTapNotice} />;
            })}
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  navigation: {
    flexDirection: "row",
    flex: 2,
  },
  cuadro: {
    backgroundColor: "#00609C",
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    position: "absolute",
    height: 220,
    width: screenWidth,
  },
  body: {
    flex: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  absolute: {
    marginTop: 70,
    marginLeft: 140,
  },
});

export { _HomeScreen };
